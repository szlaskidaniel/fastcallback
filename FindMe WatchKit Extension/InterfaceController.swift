//
//  InterfaceController.swift
//  FindMe WatchKit Extension
//
//  Created by Daniel Szlaski on 12.02.2016.
//  Copyright © 2016 Daniel Szlaski. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {

    var mapLocation: CLLocationCoordinate2D?
    var sParticipantID: String = ""
    private var CICSession: mCIC!
    var bUsed = false
    
    
    @IBOutlet var viewMap: WKInterfaceMap!
    @IBOutlet var btnRequest: WKInterfaceButton!
    @IBOutlet var lblStatus: WKInterfaceLabel!
    @IBOutlet var btnSendRequest: WKInterfaceButton!
    @IBOutlet var groupMap: WKInterfaceGroup!
    @IBOutlet var groupSuccess: WKInterfaceGroup!
    @IBOutlet var imgConfirmation: WKInterfaceImage!
    @IBOutlet var lblResultAction: WKInterfaceLabel!
    
    
    override init() {
        super.init()
        if WCSession.isSupported() {
            WCSession.defaultSession().delegate = self
            WCSession.defaultSession().activateSession()

        }
        
        self.groupSuccess.setHeight(0)
        self.groupSuccess.setAlpha(0.0)
        self.groupMap.setRelativeHeight(1.0, withAdjustment: 0.0)
        self.groupMap.setAlpha(1.0)
        btnSendRequest.setHidden(true)
        
        viewMap.setHidden(true)
        btnRequest.setHidden(true)
        lblStatus.setHidden(false)
        lblStatus.setText("")
        
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        
    }
    
    @IBAction func btnSend() {
       
        //Only need to check reachable calling from the iPhone
        guard WCSession.defaultSession().reachable else { return }
        
        WCSession.defaultSession().sendMessage(["Key" : "DeleteCB", "ID" : sParticipantID],
            
            replyHandler: { reply in print("OK")},
            
            errorHandler: { error in
                print("WatchOS: Error")
                self.lblStatus.setText("iPhone not available")
        })
        
    }
    
    func RequestSend () {
        //CICSession = mCIC.sharedInstance
        
        self.groupSuccess.setHeight(0)
        self.groupSuccess.setAlpha(0.0)
        self.groupMap.setRelativeHeight(1.0, withAdjustment: 0.0)
        self.groupMap.setAlpha(1.0)
        btnSendRequest.setHidden(true)
        
        viewMap.setHidden(true)
        btnRequest.setHidden(true)
        lblStatus.setHidden(false)
        lblStatus.setText("Locating... pls wait")
        
        groupMap.setRelativeHeight(1.0, withAdjustment: 0.0)
        groupSuccess.setRelativeHeight(0.0, withAdjustment: 0.0)
        
        sendMessage()

    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()

            RequestSend()
    }
    
    
    func sendMessage() {
        
        //Only need to check reachable calling from the iPhone
        guard WCSession.defaultSession().reachable else { return }
        
        WCSession.defaultSession().sendMessage(["Key" : "GetMapLocation"],
            
            replyHandler: { reply in print("OK")},
            
            errorHandler: { error in
            print("WatchOS: Error")
            self.lblStatus.setText("iPhone not available")
        })
        print("Request sent")
    }
    
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        
        print("WatchOS: Got new data")
        replyHandler(["Status": "OK"])
        
        if ((message["Status"] as? String)?.characters.count > 0) {
            
            self.lblStatus.setText(message["Status"] as? String)
            return
        }
        
        if ((message["Deleted"] as? String)?.characters.count > 0) {
            
  
            btnRequest.setHidden(false)
            
            WKInterfaceDevice.currentDevice().playHaptic(WKHapticType.Success)
            self.groupSuccess.setAlpha(1.0)
            self.lblResultAction.setText("Callback deleted")
    
            
            animateWithDuration(0.35, animations: { () -> Void in

                self.groupMap.setHeight(0)
                self.groupMap.setAlpha(0.0)
                self.groupSuccess.setRelativeHeight(1.0, withAdjustment: 0.0)
                        self.btnSendRequest.setHidden(true)
                         self.viewMap.setHidden(true)
            })

            
            return
        }
        
        
        self.btnSendRequest.setHidden(false)
        self.lblStatus.setText("Fast Callback")
        viewMap.setHidden(false)
        btnRequest.setHidden(false)
        btnSendRequest.setHidden(false)
        sParticipantID = message["ID"] as! String
        
        let lat = message["lat"] as! Double
        let long = message["long"] as! Double
        

        self.mapLocation = CLLocationCoordinate2DMake(lat, long)
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let region = MKCoordinateRegionMake(self.mapLocation!, span)
        self.viewMap.setRegion(region)
        self.viewMap.addAnnotation(self.mapLocation!, withPinColor: .Red)
        
       
        WKInterfaceDevice.currentDevice().playHaptic(WKHapticType.Success)
        self.groupSuccess.setAlpha(1.0)
        self.lblResultAction.setText("Request sent")
        
        animateWithDuration(0.35, animations: { () -> Void in
            
            self.groupMap.setHeight(0)
            self.groupMap.setAlpha(0.0)
            self.groupSuccess.setRelativeHeight(1.0, withAdjustment: 0.0)
        })
        

        
    }


    @IBAction func btnRequestCB() {
    
       // Hide Screen
        
        animateWithDuration(0.35, animations: { () -> Void in
            // 3
            
            self.groupMap.setHeight(190)
            self.groupMap.setAlpha(1.0)
            self.groupSuccess.setAlpha(0.0)
            
            self.groupSuccess.setRelativeHeight(-1.0, withAdjustment: 0.0)
        })
        

       
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
