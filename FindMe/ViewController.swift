//
//  ViewController.swift
//  FindMe
//
//  Created by Daniel Szlaski on 12.02.2016.
//  Copyright © 2016 Daniel Szlaski. All rights reserved.
//

import UIKit
import WatchConnectivity
import CoreLocation




class ViewController: UIViewController, WCSessionDelegate, CLLocationManagerDelegate, mCIC_Delegate {
    
    
    
    enum eOriginSource {
        case iPhone, iWatch
    }
    
    var tOrigin: eOriginSource = .iWatch
    var bActiveRequest = false

    let locationManager = CLLocationManager()
    var replyValues = Dictionary<String, AnyObject>()
    
    private var CICSession: mCIC!
    
    var CB_SentDate = NSDate()
    
    let dateFormatter = NSDateFormatter()
   

    
    @IBOutlet weak var lblError: UITextView!
    
    @IBOutlet weak var lblCallBackID: UILabel!
    @IBOutlet weak var btnDeleteCB: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if WCSession.isSupported() {
            WCSession.defaultSession().delegate = self
            WCSession.defaultSession().activateSession()
        }
        locationManager.requestAlwaysAuthorization()
        [replyValues .removeAll()]
        
    }
    
    override func viewWillAppear(animated: Bool) {
    
        CICSession = mCIC.sharedInstance
        CICSession.delegate = self

        if (CICSession._CB_ParticipantID.characters.count < 1) {
            self.lblCallBackID.text = ""
            self.btnDeleteCB.hidden = true
        } else {
            self.lblCallBackID.text = CICSession._CB_ParticipantID
            self.btnDeleteCB.hidden = false
        }
        
    }
    
    @IBAction func btnDial(sender: AnyObject) {
        
        // Check phone Dial
        if let url = NSURL(string: "tel://226220702,1231231231231") {
            UIApplication.sharedApplication().openURL(url)
        }
        
        
    }
    @IBAction func btnSend(sender: AnyObject) {


        
        tOrigin = .iPhone
        self.FindMe()
    
    }
    
    
    func SendCallback (aMessage: String) {

        var sWorkgroup = NSUserDefaults.standardUserDefaults().stringForKey("_WORKGROUP")
        var sUserName = NSUserDefaults.standardUserDefaults().stringForKey("_USERNAME")
        var sPhone = NSUserDefaults.standardUserDefaults().stringForKey("_PHONE")
        
        if (sWorkgroup == nil) {sWorkgroup = ""}
        if (sUserName == nil) {sUserName = ""}
        if (sPhone == nil) {sPhone = ""}
        
        do {
            try CICSession.CreateCallBack(sWorkgroup!, sSubject: aMessage, sUserName: sUserName!, sPhoneNumber: sPhone!)
            
        }
        catch mCIC_ErrorType.InvalidJSON {
            print("Wrong data passed")
        } catch {
            // Default
        }
        
    }
    
    
    @IBAction func btnDeleteLastCallBack(sender: AnyObject) {
        
        tOrigin = .iPhone
        CICSession.DeleteCallBack(CICSession._CB_ParticipantID)
        
    }
    
    func FindMe () {
        
        bActiveRequest = true
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Location update")
        locationManager.stopUpdatingLocation()
        let currentLocation = locations[locations.count - 1]
        
        replyValues["lat"] = currentLocation.coordinate.latitude
        replyValues["long"] = currentLocation.coordinate.longitude
        
        let sMessage = "\(currentLocation.coordinate.latitude)|\(currentLocation.coordinate.longitude)"

        if (bActiveRequest) {
            bActiveRequest = false
            SendCallback(sMessage)
        }

        
    }
    
   
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        //
        
        replyValues.removeAll()
        
            if(message["Key"] as! String == "DeleteCB") {
            let sID = message["ID"] as! String
            CICSession.DeleteCallBack(sID)
            
            
        } else {
            print("iOS: Start to Find Location...")
            tOrigin = .iWatch
            FindMe()
        }
        
        //replyHandler(["Status": "OK"])
    }

    
    
    func sendMessage(msg: [String : AnyObject]) {
        guard WCSession.defaultSession().reachable else { return }
        
        WCSession.defaultSession().sendMessage(msg,
            
            replyHandler: { reply in
                //Handle Reply
                print("iOS: Message sent")
                
            },
            
            errorHandler: { error in
                //Handle Error
                print("iOS: Error")
        })
    }
    
    func ShowMessageAlert(aText: String) {
        
        let alertController = UIAlertController(title: "-Message-", message: aText, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func mCIC_Error(sDescription: NSString) {
        print(sDescription)
        
        if (tOrigin == .iWatch) {
            replyValues["Status"] = "Error..."
            sendMessage(replyValues)
        } else {
            dispatch_async(dispatch_get_main_queue(), {
                self.ShowMessageAlert(sDescription as String)
            })
        }
        
    }
    
    func mCIC_CreateCallback_Success(sParticipantID: String?) {

        if (tOrigin == .iWatch) {
            [replyValues .removeValueForKey("Status")]
            replyValues["ID"] = sParticipantID
            sendMessage(replyValues)

                   } else {
            dispatch_async(dispatch_get_main_queue(), {
                self.btnDeleteCB.hidden = false
                self.lblCallBackID.text = sParticipantID
                self.ShowMessageAlert("Callback sent")
                
            })
        }
    }
    
    func mCIC_DeleteCallBack_Success() {
        
         if (tOrigin == .iWatch) {
            [replyValues .removeValueForKey("Status")]
            replyValues["Deleted"] = "true"
            sendMessage(replyValues)
            
         } else {
        
            dispatch_async(dispatch_get_main_queue(), {
                self.ShowMessageAlert("Callback deleted !")
                self.lblCallBackID.text = ""
                self.btnDeleteCB.hidden = true
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

