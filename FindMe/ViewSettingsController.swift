//
//  ViewSettingsController.swift
//  FindMe
//
//  Created by Daniel Szlaski on 17.03.2016.
//  Copyright © 2016 Daniel Szlaski. All rights reserved.
//

import Foundation
import UIKit


class ViewSettingsController:UIViewController {
    
    @IBOutlet weak var txtIP: UITextField!
    @IBOutlet weak var txtPort: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtWorkgroup: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtIP.text = NSUserDefaults.standardUserDefaults().stringForKey("_IP")
        txtPort.text = NSUserDefaults.standardUserDefaults().stringForKey("_PORT")
        txtUserName.text = NSUserDefaults.standardUserDefaults().stringForKey("_USERNAME")
        txtPhone.text = NSUserDefaults.standardUserDefaults().stringForKey("_PHONE")
        txtWorkgroup.text = NSUserDefaults.standardUserDefaults().stringForKey("_WORKGROUP")
        
    }
    
    override func viewWillDisappear(animated: Bool) {
      
        NSUserDefaults.standardUserDefaults().setObject(txtIP.text, forKey: "_IP")
        NSUserDefaults.standardUserDefaults().setObject(txtPort.text, forKey: "_PORT")
        NSUserDefaults.standardUserDefaults().setObject(txtUserName.text, forKey: "_USERNAME")
        NSUserDefaults.standardUserDefaults().setObject(txtPhone.text, forKey: "_PHONE")
        NSUserDefaults.standardUserDefaults().setObject(txtWorkgroup.text, forKey: "_WORKGROUP")
        
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}